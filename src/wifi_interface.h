/* vim:set expandtab ts=4 shiftwidth=4: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * wifi_interface.h
 *
 */

#ifndef WIFI_INTERFACE_H_
#define WIFI_INTERFACE_H_

#include "connman-interface/connman_interface.h"

GVariant *get_wifi_network_list (void);

void
remove_from_wifi_network (const gchar *arg_network_id);

#endif /* WIFI_INTERFACE_H_ */

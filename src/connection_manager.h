/* vim:set expandtab ts=4 shiftwidth=4: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CONNECTION_MANAGER_H_
#define CONNECTION_MANAGER_H_

/* NetworkType */
#define BECKFOOT_NETWORK_TYPE_BLUETOOTH 	"Network-Type-Bluetooth"
#define BECKFOOT_NETWORK_TYPE_LAN			"Network-Type-LAN"
#define BECKFOOT_NETWORK_TYPE_WIFI		"Network-Type-Wifi"
#define BECKFOOT_NETWORK_TYPE_3G			"Network-Type-3G"
#define BECKFOOT_NETWORK_TYPE_WIMAX       "Network-Type-Wimax"
#define BECKFOOT_NETWORK_TYPE_UNKNOWN     "Network-Type-Unknown"

/* NetworkState */
#define BECKFOOT_NETWORK_STATE_AVAILABLE 	"Network-State-Available"
#define BECKFOOT_NETWORK_STATE_ASSOCIATED "Network-State-Associated"
#define BECKFOOT_NETWORK_STATE_CONNECTED	"Network-State-Connected"
#define BECKFOOT_NETWORK_STATE_DISABLED	"Network-State-Disabled"
#define BECKFOOT_NETWORK_STATE_UNKNOWN	"Network-State-Unknown"
#define BECKFOOT_NETWORK_STATE_ONLINE "Online"
#define BECKFOOT_NETWORK_STATE_OFFLINE ""
#define BECKFOOT_NETWORK_STATE_READY  "ready"

/* NetworkName */
#define BECKFOOT_NETWORK_NAME_UNKNOWN  "Network-Name-Unknown"

#endif /* CONNECTION_MANAGER_H_ */

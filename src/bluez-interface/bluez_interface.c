/* vim:set expandtab ts=4 shiftwidth=4: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * bluez_interface.c
 *
 *  Created on: Mar 11, 2013
 */

#include "bluez_interface.h"
#include "../connman-interface/connman_interface.h"

#define BLUEZ_INTERFACE_DEBUG 0
#if BLUEZ_INTERFACE_DEBUG
#define BLUEZ_INTERFACE_PRINT(...)       g_print(__VA_ARGS__)
#else
#define BLUEZ_INTERFACE_PRINT(...)
#endif

guint bluez_service_debug_flags = 0;

static const GDebugKey bluez_debug_keys[] =
  {
    { "bluez-mgr", BLUEZ_SRV_DEBUG } };

static void
custom_ghashtable_free (gpointer pvalue)
{
  if (pvalue)
    {
      g_free (pvalue);
      pvalue = NULL;
    }
}

void
replace_string (gchar *orig_str, gchar *mod_str)
{
  int count, i = 0;
  int len = strlen (orig_str);
  for (count = 0; count < len; count++)
    {
      if (orig_str[count] == '_')
        {
          mod_str[i] = ':';
          i++;
        }
      else
        {
          mod_str[i] = orig_str[count];
          i++;
        }
    }
  mod_str[i] = '\0';
}

gint
list_custome_search (gconstpointer a, gconstpointer b)
{
  gchar *success_ptr = NULL;

  success_ptr = g_strrstr ((gchar *) a, (gchar *) b);
  if (success_ptr != NULL)
    {
      return 0;
    }
  else
    {
      return -1;
    }
}

static void
service_path_glist_custom_free (gpointer data)
{
  if (data)
    {
      g_free (data);
      data = NULL;
    }
}

GList *
filter_for_bluetooth_list (GVariant *network_list)
{
  GList *service_path_list = NULL;
  if (network_list != NULL)
    {
      gchar *name2 = NULL;
      GVariantIter iter, array_iter;
      GVariant *array = NULL, *array_value = NULL;
      GVariantIter *fields;

      g_variant_iter_init (&iter, network_list);
      array = g_variant_iter_next_value (&iter);
      g_variant_iter_init (&array_iter, array);
      while ((array_value = g_variant_iter_next_value (&array_iter)) != NULL)
        {
          g_variant_get (array_value, "(oa{sv})", &name2, &fields);
          if (g_strrstr (name2, "bluetooth"))
            {
              BLUEZ_DNLD_PRINT(" Object Path %s\n", name2);
              service_path_list = g_list_append (service_path_list,
                                                 g_strdup (name2));
            }
        }

    }
  service_path_list = g_list_first (service_path_list);
  return service_path_list;
}

void
remove_colon_from_string (gchar *orig_str, gchar *mod_str)
{
  int count, i = 0;
  int len = strlen (orig_str);
  for (count = 0; count < len; count++)
    {
      if (orig_str[count] == ':')
        {
        }
      else
        {
          mod_str[i] = tolower (orig_str[count]);
          i++;
        }
    }
  mod_str[i] = '\0';
}
